## Description

Node.js server for an awesome promotions app developed for the Coca Cola Company

## Installation

```bash
$ npm install
```
## Setting up the environment
You need to create a .env that looks like: <br/>

```
NODE_ENV=development
SOURCE_PATH=src
PORT=3000
JWT_SECRET=secretkey
JWT_EXPIRE=144000
TYPEORM_HOST=localhost
TYPEORM_PORT=3306
TYPEORM_CONNECTION=mysql
TYPEORM_USERNAME=dev
TYPEORM_PASSWORD=player
TYPEORM_DATABASE=testdb
TYPEORM_ENTITIES=src/database/entities/*.ts
TYPEORM_MIGRATIONS=src/database/migration/*.ts
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

