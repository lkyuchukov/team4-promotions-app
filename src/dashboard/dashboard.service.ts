import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as datefns from 'date-fns';
import { MoreThan, Repository } from 'typeorm';
import { ActivityEntity } from '../database/entities/activity.entity';
import { CodeEntity } from '../database/entities/code.entity';
import { CustomerEntity } from '../database/entities/customer.entity';
import { OutletEntity } from '../database/entities/outlet.entity';
import { RedemptionRecordEntity } from '../database/entities/redemption-record.entity';
import { UserEntity } from '../database/entities/user.entity';
import { activityResponse } from '../shared/activity-response';

@Injectable()
export class DashboardService {
  constructor(
    @InjectRepository(ActivityEntity)
    private activityRepository: Repository<ActivityEntity>,

    @InjectRepository(CodeEntity)
    private codeRepository: Repository<CodeEntity>,

    @InjectRepository(RedemptionRecordEntity)
    private redemptionRecordRepository: Repository<RedemptionRecordEntity>,

    @InjectRepository(CustomerEntity)
    private customerRepository: Repository<CustomerEntity>,

    @InjectRepository(OutletEntity)
    private outletRepository: Repository<OutletEntity>,

    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async getAllActivity() {
    const activities = await this.activityRepository.find({
      order: { createdOn: 'DESC' },
    });

    const response = activities.map(
      async activity => await activityResponse(activity),
    );

    return Promise.all(response);
  }

  async getPrizesCount() {
    const smallPrizesCount = await this.codeRepository
      .createQueryBuilder('code')
      .innerJoinAndSelect('code.prize', 'prize')
      .where('prize.prize = :alias', { alias: 'Small prize' })
      .andWhere('code.isRedeemed = :isRedeemed', { isRedeemed: true })
      .getCount();

    const mediumPrizesCount = await this.codeRepository
      .createQueryBuilder('code')
      .innerJoinAndSelect('code.prize', 'prize')
      .where('prize.prize = :alias', { alias: 'Medium prize' })
      .andWhere('code.isRedeemed = :isRedeemed', { isRedeemed: true })
      .getCount();

    const bigPrizesCount = await this.codeRepository
      .createQueryBuilder('code')
      .innerJoinAndSelect('code.prize', 'prize')
      .where('prize.prize = :alias', { alias: 'Big prize' })
      .andWhere('code.isRedeemed = :isRedeemed', { isRedeemed: true })
      .getCount();

    return {
      smallPrizesCount,
      mediumPrizesCount,
      bigPrizesCount,
    };
  }

  async getDetailedPrizeInfo() {
    const startOfDay = datefns.startOfToday();
    const redemptions = await this.redemptionRecordRepository.find({
      relations: ['code'],
      where: { redeemedOn: MoreThan(startOfDay) },
      order: {
        createdOn: 'ASC',
      },
    });

    const res = Promise.all(
      redemptions.map(record => Promise.resolve(record.code)),
    );

    return res.then(data =>
      data.reduce((allPrizes, prize) => {
        if (prize.prize.prize in allPrizes) {
          allPrizes[prize.prize.prize]++;
        } else {
          allPrizes[prize.prize.prize] = 1;
        }
        return allPrizes;
      }, {}),
    );
  }

  async getDetailedRedemptionsInfo() {
    const startOfDay = datefns.startOfToday();
    const redemptions = await this.redemptionRecordRepository.find({
      where: { redeemedOn: MoreThan(startOfDay) },
      order: {
        createdOn: 'ASC',
      },
    });

    const res = redemptions
      .map(record => datefns.getHours(record.createdOn))
      .reduce((allRecords, record) => {
        if (record in allRecords) {
          allRecords[record]++;
        } else {
          allRecords[record] = 1;
        }
        return allRecords;
      }, {});

    return res;
  }

  async getRedemptionsInfoForToday() {
    const startOfDay = datefns.startOfToday();

    const redemptions = await this.redemptionRecordRepository.find({
      where: { redeemedOn: MoreThan(startOfDay) },
      order: {
        createdOn: 'ASC',
      },
    });

    return redemptions;
  }

  async getRedemptionsInfoForThisWeek() {
    const startOfWeek = datefns.startOfWeek(new Date(Date.now()));

    const redemptions = await this.redemptionRecordRepository.find({
      redeemedOn: MoreThan(startOfWeek),
    });

    return redemptions;
  }

  async getRedemptionsInfoForThisMonth() {
    const startOfMonth = datefns.startOfMonth(new Date(Date.now()));

    const redemptions = await this.redemptionRecordRepository.find({
      redeemedOn: MoreThan(startOfMonth),
    });

    return redemptions;
  }

  async getEntityNumbers() {
    const customerCount = await this.customerRepository.count();
    const outletCount = await this.outletRepository.count();
    const userCount = await this.userRepository.count();
    return { customerCount, outletCount, userCount };
  }
}
