import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../common/decorators/roles.decorator';
import { UserRole } from '../common/enums/roles.enum';
import { RolesGuard } from '../common/guards/roles.guard';
import { DashboardService } from './dashboard.service';

@Controller('dashboard')
export class DashboardController {
  constructor(private dashboardService: DashboardService) {}

  @Get('')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async getAllActivity() {
    const activity = await this.dashboardService.getAllActivity();
    const prizeInfo = await this.dashboardService.getPrizesCount();
    const redemptionsToday = await this.dashboardService.getRedemptionsInfoForToday();
    const redemptionsThisWeek = await this.dashboardService.getRedemptionsInfoForThisWeek();
    const redemptionsThisMonth = await this.dashboardService.getRedemptionsInfoForThisMonth();
    const entityCount = await this.dashboardService.getEntityNumbers();
    const detailedRedemptionsInfo = await this.dashboardService.getDetailedRedemptionsInfo();
    const detailedPrizeInfo = await this.dashboardService.getDetailedPrizeInfo();

    return {
      activity,
      prizeInfo,
      redemptionsToday,
      redemptionsThisWeek,
      redemptionsThisMonth,
      entityCount,
      detailedRedemptionsInfo,
      detailedPrizeInfo,
    };
  }
}
