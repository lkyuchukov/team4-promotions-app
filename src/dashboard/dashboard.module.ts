import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { ActivityEntity } from '../database/entities/activity.entity';
import { UserEntity } from '../database/entities/user.entity';
import { ActivityService } from '../shared/activity.service';
import { DashboardController } from './dashboard.controller';
import { DashboardService } from './dashboard.service';
import { CodeEntity } from '../database/entities/code.entity';
import { RedemptionRecordEntity } from '../database/entities/redemption-record.entity';
import { OutletEntity } from '../database/entities/outlet.entity';
import { CustomerEntity } from '../database/entities/customer.entity';

@Module({
  imports: [
    AuthModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    TypeOrmModule.forFeature([
      ActivityEntity,
      UserEntity,
      CodeEntity,
      RedemptionRecordEntity,
      CustomerEntity,
      OutletEntity,
      UserEntity,
    ]),
  ],
  providers: [DashboardService, ActivityService],
  controllers: [DashboardController],
})
export class DashboardModule {}
