import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { UserEntity } from '../database/entities/user.entity';
import { LoginUserDTO } from '../models/user/login-user';
import { UserDTO } from '../models/user/user';
import { toAuthResponseObject } from './auth-response';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    private jwtService: JwtService,
  ) {}

  public async getUserByUsername(username: string): Promise<UserDTO> {
    const user = await this.userRepository.findOne({ where: { username } });
    if (!user) {
      throw new NotFoundException();
    }

    return toAuthResponseObject(user);
  }

  public async validateUserPassword(userData: LoginUserDTO) {
    const { username, password } = userData;
    const userEntity = await this.userRepository.findOne({
      where: { username },
    });

    return await bcrypt.compare(password, userEntity.password);
  }

  public async login(userData: LoginUserDTO) {
    const { username } = userData;
    const user = await this.userRepository.findOne({ where: { username } });
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'No such user exists',
        },
        404,
      );
    }

    const payload = { username };
    const passwordMatch = await this.validateUserPassword(userData);
    if (!passwordMatch) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'Wrong password',
        },
        404,
      );
    }

    user.isLoggedIn = true;
    await this.userRepository.save(user);

    const token = await this.jwtService.signAsync(payload);

    return { token, user: toAuthResponseObject(user) };
  }

  public async logout(userData: UserDTO) {
    const { id } = userData;
    const user = await this.userRepository.findOne({ where: { id } });
    user.isLoggedIn = false;

    await this.userRepository.save(user);

    return toAuthResponseObject(user);
  }
}
