import {
  Body,
  Controller,
  Delete,
  HttpStatus,
  Post,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TokenDecorator } from '../common/decorators/token.decorator';
import { UserDecorator } from '../common/decorators/user.decorator';
import { LoginUserDTO } from '../models/user/login-user';
import { UserDTO } from '../models/user/user';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  public async loginUser(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    user: LoginUserDTO,
  ) {
    return await this.authService.login(user);
  }

  @Delete('logout')
  @UseGuards(AuthGuard())
  public async logoutUser(
    @UserDecorator() userData: UserDTO,
    @TokenDecorator() token: string,
  ) {
    await this.authService.logout(userData);
    return {
      statusCode: HttpStatus.OK,
    };
  }
}
