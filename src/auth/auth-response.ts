import { UserEntity } from '../database/entities/user.entity';

export function toAuthResponseObject(user: UserEntity) {
  const { id, username, roles, createdOn, updatedOn, isLoggedIn, isDeleted } = user;

  const responseObject = {
    id,
    username,
    roles,
    createdOn,
    updatedOn,
    isLoggedIn,
    isDeleted,
  };

  return responseObject;
}
