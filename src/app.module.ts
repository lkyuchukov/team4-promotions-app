import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { CodeModule } from './code/code.module';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { CustomerModule } from './customer/customer.module';
import { IssueModule } from './issue/issue.module';
import { OutletModule } from './outlet/outlet.module';
import { UserModule } from './user/user.module';
import { ActivityService } from './shared/activity.service';
import { DashboardModule } from './dashboard/dashboard.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        host: configService.dbHost,
        type: configService.dbType as any,
        database: configService.dbName,
        port: configService.dbPort,
        username: configService.dbUsername,
        password: configService.dbPassword,
        synchronize: true,
        migrations: [
          `${configService.sourcePath}/database/migration/**/*{.ts,.js}`,
        ],
        entities: [
          `${configService.sourcePath}/database/entities/*{.ts,.js}`,
        ],
        cli: {
          entitiesDir: `${configService.sourcePath}/database/entities`,
          migrationsDir: `${configService.sourcePath}/database/migration`,
        },
      }),
    }),
    ConfigModule,
    AuthModule,
    CodeModule,
    CustomerModule,
    UserModule,
    OutletModule,
    IssueModule,
    DashboardModule,
  ],
})
export class AppModule {}
