import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from '../database/entities/user.entity';
import { CreateReportDTO } from '../models/report/create-report.dto';
import { UserDTO } from '../models/user/user';
import { ActivityService } from '../shared/activity.service';
import { sendReportMail } from './send-report-mail';

@Injectable()
export class ReportService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    private activityService: ActivityService,
  ) {}
  async reportMultipleRedemptionAttempt(
    userData: UserDTO,
    reportData: CreateReportDTO,
  ) {
    const { id } = userData;
    const user = await this.userRepository.findOne({
      where: { id },
      relations: ['outlet'],
    });

    const outlet = await user.outlet;
    reportData.address = outlet.address;
    reportData.city = outlet.city;

    this.activityService.reportMultipleRedemptionAttempt(user);

    sendReportMail(userData, reportData).catch(Logger.error);
  }
}
