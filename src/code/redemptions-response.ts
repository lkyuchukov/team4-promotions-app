import { RedemptionRecordEntity } from '../database/entities/redemption-record.entity';
import { userToResponseObject } from '../user/user-response';

export async function recordToResponseObject(
  redemptionRecord: RedemptionRecordEntity,
) {
  const outlet = await redemptionRecord.outlet;
  return {
    id: redemptionRecord.id,
    createdOn: redemptionRecord.createdOn,
    redeemedOn: redemptionRecord.redeemedOn,
    code: await redemptionRecord.code,
    user: await userToResponseObject(await redemptionRecord.user),
    outlet,
    customer: await outlet.customer,
  };
}
