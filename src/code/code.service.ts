import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CodeEntity } from '../database/entities/code.entity';
import { RedemptionRecordEntity } from '../database/entities/redemption-record.entity';
import { UserEntity } from '../database/entities/user.entity';
import { UserDTO } from '../models/user/user';
import { ActivityService } from '../shared/activity.service';
import { recordToResponseObject } from './redemptions-response';

@Injectable()
export class CodeService {
  constructor(
    @InjectRepository(CodeEntity)
    private codeRepository: Repository<CodeEntity>,

    @InjectRepository(RedemptionRecordEntity)
    private redemptionRecordRepository: Repository<RedemptionRecordEntity>,

    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    private activityService: ActivityService,
  ) {}

  async checkIfCodeIsValid(value: string) {
    const codeToRedeem = await this.codeRepository.findOne({
      where: { value },
    });

    if (!codeToRedeem) {
      return {
        status: 'Invalid',
        value,
      };
    }

    if (codeToRedeem.isRedeemed === true) {
      return {
        status: 'Redeemed',
        value,
        prize: codeToRedeem.prize,
      };
    }

    return {
      status: 'Valid',
      value,
      prize: codeToRedeem.prize,
    };
  }

  async redeemCode(userData: UserDTO, codeData: { code: string }) {
    const { code } = codeData;
    const codeToRedeem = await this.codeRepository.findOne({
      where: { value: code },
    });
    if (codeToRedeem.isRedeemed === true) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          message: 'This code has already been redeemed',
        },
        409,
      );
    }

    codeToRedeem.isRedeemed = true;

    const { id } = userData;
    const userRedeemingCode = await this.userRepository.findOne({
      where: { id },
      relations: ['outlet'],
    });

    if (!userRedeemingCode) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          message: `Sorry, something went wrong`,
        },
        400,
      );
    }

    const redemptionRecord = new RedemptionRecordEntity();
    redemptionRecord.code = Promise.resolve(codeToRedeem);
    redemptionRecord.user = Promise.resolve(userRedeemingCode);
    redemptionRecord.redeemedOn = new Date(Date.now());
    redemptionRecord.outlet = Promise.resolve(userRedeemingCode.outlet);

    await this.codeRepository.save(codeToRedeem);
    await this.redemptionRecordRepository.save(redemptionRecord);

    this.activityService.redeemCode(userRedeemingCode);

    return redemptionRecord;
  }

  async getAllRedeemedCodes() {
    const redeemedCodes = await this.redemptionRecordRepository.find({
      relations: ['user', 'outlet', 'outlet.customer', 'code'],
    });

    const response = redeemedCodes.map(
      async record => await recordToResponseObject(record),
    );

    return Promise.all(response);
  }
}
