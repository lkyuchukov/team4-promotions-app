import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { ConfigModule } from '../config/config.module';
import { ActivityEntity } from '../database/entities/activity.entity';
import { CodeEntity } from '../database/entities/code.entity';
import { RedemptionRecordEntity } from '../database/entities/redemption-record.entity';
import { UserEntity } from '../database/entities/user.entity';
import { ActivityService } from '../shared/activity.service';
import { CodeController } from './code.controller';
import { CodeService } from './code.service';
import { ReportController } from './report.controller';
import { ReportService } from './report.service';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    AuthModule,
    TypeOrmModule.forFeature([
      UserEntity,
      RedemptionRecordEntity,
      CodeEntity,
      ActivityEntity,
    ]),
    ConfigModule,
  ],
  providers: [CodeService, ReportService, ActivityService],
  controllers: [CodeController, ReportController],
})
export class CodeModule {}
