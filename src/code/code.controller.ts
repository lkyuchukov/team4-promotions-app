import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserDecorator } from '../common/decorators/user.decorator';
import { UserDTO } from '../models/user/user';
import { CodeService } from './code.service';

@Controller('codes')
export class CodeController {
  constructor(private codeService: CodeService) {}

  @Get()
  @UseGuards(AuthGuard())
  async getAllRedeemedCodes() {
    return this.codeService.getAllRedeemedCodes();
  }

  @Get(':code')
  @UseGuards(AuthGuard())
  async checkIfCodeIsValid(@Param('code') code: string) {
    return this.codeService.checkIfCodeIsValid(code);
  }

  @Post('')
  @UseGuards(AuthGuard())
  async redeemCode(
    @UserDecorator() userData: UserDTO,
    @Body() codeData: { code: string },
  ) {
    return this.codeService.redeemCode(userData, codeData);
  }
}
