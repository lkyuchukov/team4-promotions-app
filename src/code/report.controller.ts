import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserDecorator } from '../common/decorators/user.decorator';
import { UserDTO } from '../models/user/user';
import { ReportService } from './report.service';
import { CreateReportDTO } from '../models/report/create-report.dto';

@Controller('reports')
export class ReportController {
  constructor(private reportService: ReportService) {}

  @Post()
  @UseGuards(AuthGuard())
  async reportMultileRedemptionAttempt(
    @UserDecorator() userData: UserDTO,
    @Body() reportData: CreateReportDTO,
  ) {
    return this.reportService.reportMultipleRedemptionAttempt(
      userData,
      reportData,
    );
  }
}
