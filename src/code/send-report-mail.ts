import { Logger } from '@nestjs/common';
import * as nodemailer from 'nodemailer';
import { UserDTO } from '../models/user/user';
import { CreateReportDTO } from '../models/report/create-report.dto';

export async function sendReportMail(userData: UserDTO, reportData: CreateReportDTO) {
  const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    auth: {
      user: 'judge.becker@ethereal.email',
      pass: 'VVDK8sW3upgMJ5hyam',
    },
  });

  const info = await transporter.sendMail({
    from: `${userData.username}@promotions.com`,
    to: 'support@promotions.com',
    subject: 'Multiple redemption attempt',
    html: `<h3>Multiple redemption attempt!</h3>
    <h3>Code: ${reportData.value}</h3>
    <h3>Outlet address: ${reportData.address}</h3>
    <h3>City: ${reportData.city}</h3>
    `,
  });

  Logger.log('Message sent: %s', info.messageId);
}
