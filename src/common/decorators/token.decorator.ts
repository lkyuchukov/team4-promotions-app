import { createParamDecorator } from '@nestjs/common';

export const TokenDecorator = createParamDecorator((data, req) => {
  return req.headers.authorization;
});
