export enum ActivityEnum {
  Create,
  Update,
  Delete,
  Redeem,
  Report,
}
