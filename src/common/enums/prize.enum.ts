export enum PrizeEnum {
  Small = 'Small prize',
  Medium = 'Medium prize',
  Big = 'Big prize',
}
