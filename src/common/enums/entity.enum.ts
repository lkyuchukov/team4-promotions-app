export enum EntityEnum {
  Customer,
  Outlet,
  User,
  Code,
  MultipleRedemptionAttempt,
}
