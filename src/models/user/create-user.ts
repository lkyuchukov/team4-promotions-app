import { IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateUserDTO {
  @IsNotEmpty()
  @IsString()
  @Length(5, 25)
  username: string;

  @IsNotEmpty()
  @IsString()
  @Length(5, 25)
  password: string;

  @IsNotEmpty()
  @IsString()
  outletId: string;
}
