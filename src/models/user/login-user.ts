import { IsNotEmpty, IsString, Length } from 'class-validator';

export class LoginUserDTO {
  @IsNotEmpty()
  @IsString()
  @Length(5, 25)
  username: string;

  @IsNotEmpty()
  @IsString()
  @Length(5, 25)
  password: string;
}
