import { OutletEntity } from '../../database/entities/outlet.entity';
import { RoleEntity } from '../../database/entities/role.entity';

export class UserDTO {
  id: string;
  username: string;
  roles: RoleEntity[];
  isLoggedIn: boolean;
  createdOn: Date;
  password?: string;
  updatedOn: Date;
  outlet?: OutletEntity;
}
