import { IsNotEmpty, IsString, Length } from 'class-validator';

export class SubmitIssueDTO {
  @IsNotEmpty()
  @IsString()
  @Length(5, 35)
  title: string;

  @IsNotEmpty()
  @IsString()
  @Length(5, 200)
  description: string;
}
