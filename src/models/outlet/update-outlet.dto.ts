import { IsAlpha, IsNotEmpty, IsString } from 'class-validator';

export class UpdateOutletDTO {
  @IsNotEmpty()
  @IsString()
  address: string;

  @IsNotEmpty()
  @IsString()
  @IsAlpha()
  city: string;
}
