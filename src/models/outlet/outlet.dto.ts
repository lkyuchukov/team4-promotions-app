import { IsAlpha, IsNotEmpty, IsString } from 'class-validator';

export class OutletDTO {
  @IsNotEmpty()
  @IsString()
  address: string;

  @IsNotEmpty()
  @IsString()
  @IsAlpha()
  city: string;

  @IsNotEmpty()
  @IsString()
  @IsAlpha()
  customer: string;
}
