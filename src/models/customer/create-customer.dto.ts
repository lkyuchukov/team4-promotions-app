import { IsAlpha, IsNotEmpty, IsString } from 'class-validator';

export class CreateCustomerDTO {
  @IsNotEmpty()
  @IsString()
  @IsAlpha()
  name: string;
}
