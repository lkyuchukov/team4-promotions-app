import { OutletDTO } from '../outlet/outlet.dto';

export class CustomerDTO {
  id: string;
  name: string;
  createdOn: Date;
  updatedOn: Date;
  isDeleted: boolean;
  outlets: OutletDTO[];
}
