import { IsAlpha, IsNotEmpty, IsString } from 'class-validator';

export class UpdateCustomerDTO {
  @IsNotEmpty()
  @IsString()
  @IsAlpha()
  name: string;
}
