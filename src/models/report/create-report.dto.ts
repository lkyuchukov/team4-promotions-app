export class CreateReportDTO {
  value: string;
  address: string;
  city: string;
}
