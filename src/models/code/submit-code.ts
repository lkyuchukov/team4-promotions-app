import { IsNotEmpty, IsString, Length } from 'class-validator';

export class SubmitCodeDTO {
  @IsNotEmpty()
  @IsString()
  @Length(13)
  value: string;
}
