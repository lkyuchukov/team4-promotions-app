import { NestFactory } from '@nestjs/core';
import * as helmet from 'helmet';
import { AppModule } from './app.module';
import { ConfigService } from './config/config.service';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService: ConfigService = app.get(ConfigService);

  app.enableCors();
  app.use(helmet());
  await app.listen(configService.port || 3000);
  Logger.log(`App is running on port ${configService.port}.`);
}
bootstrap();
