import { OutletEntity } from '../database/entities/outlet.entity';

export async function outletToResponseObject(outlet: OutletEntity) {
  return {
    id: outlet.id,
    address: outlet.address,
    city: outlet.city,
    isDeleted: outlet.isDeleted,
    users: await outlet.users,
    redemptionRecords: await outlet.redemptionRecords,
    customer: await outlet.customer,
  };
}
