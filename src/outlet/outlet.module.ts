import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { ActivityEntity } from '../database/entities/activity.entity';
import { CustomerEntity } from '../database/entities/customer.entity';
import { OutletEntity } from '../database/entities/outlet.entity';
import { UserEntity } from '../database/entities/user.entity';
import { ActivityService } from '../shared/activity.service';
import { OutletController } from './outlet.controller';
import { OutletService } from './outlet.service';

@Module({
  imports: [
    AuthModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    TypeOrmModule.forFeature([
      OutletEntity,
      CustomerEntity,
      ActivityEntity,
      UserEntity,
    ]),
  ],
  providers: [OutletService, ActivityService],
  controllers: [OutletController],
})
export class OutletModule {}
