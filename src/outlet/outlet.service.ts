import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CustomerEntity } from '../database/entities/customer.entity';
import { OutletEntity } from '../database/entities/outlet.entity';
import { UpdateOutletDTO } from '../models/outlet/update-outlet.dto';
import { outletToResponseObject } from './outlet-response';
import { CreateOutletDTO } from '../models/outlet/create-outlet.dto';
import { ActivityService } from '../shared/activity.service';

@Injectable()
export class OutletService {
  constructor(
    @InjectRepository(OutletEntity)
    private outletRepository: Repository<OutletEntity>,

    @InjectRepository(CustomerEntity)
    private customerRepository: Repository<CustomerEntity>,

    private activityService: ActivityService,
  ) {}

  async getAllOutlets() {
    const outlets = await this.outletRepository
      .createQueryBuilder('outlets')
      .where('outlets.isDeleted = :isDeleted', { isDeleted: false })
      .leftJoinAndMapMany(
        'outlets.users',
        'outlets.users',
        'users',
        'users.isDeleted = FALSE',
      )
      .getMany();

    const response = outlets.map(
      async outlet => await outletToResponseObject(outlet),
    );

    return Promise.all(response);
  }

  async getOutlet(id: string) {
    const outlet = await this.outletRepository
      .createQueryBuilder('outlet')
      .where('outlet.id = :id', { id })
      .leftJoinAndMapMany(
        'outlet.users',
        'outlet.users',
        'users',
        'users.isDeleted = FALSE',
      )
      .getOne();

    if (!outlet || outlet.isDeleted === true) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: 'No such outlet',
        },
        404,
      );
    }

    return outletToResponseObject(outlet);
  }

  async createOutlet(outletData: CreateOutletDTO) {
    const { address, city, customer } = outletData;

    const existingOutlet = await this.outletRepository.findOne({
      where: { address, city },
    });

    const outletCustomer = await this.customerRepository.findOne({
      where: { name: customer },
    });

    if (existingOutlet) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          message: 'Outlet already exists',
        },
        409,
      );
    }

    const newOutlet = new OutletEntity();
    newOutlet.address = address;
    newOutlet.city = city;
    newOutlet.customer = Promise.resolve(outletCustomer);

    await this.outletRepository.save(newOutlet);
    this.activityService.createOutlet();

    return outletToResponseObject(newOutlet);
  }

  async updateOutlet(id: string, outletData: UpdateOutletDTO) {
    const { address, city } = outletData;
    const existingOutlet = await this.outletRepository.findOne({
      where: { id },
    });

    if (!existingOutlet || existingOutlet.isDeleted) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: 'No such outlet',
        },
        404,
      );
    }

    if (address) {
      existingOutlet.address = address;
    }

    if (city) {
      existingOutlet.city = city;
    }

    await this.outletRepository.save(existingOutlet);
    this.activityService.editOutlet();

    return outletToResponseObject(existingOutlet);
  }

  async deleteOutlet(id: string) {
    const existingOutlet = await this.outletRepository.findOne({
      where: { id },
    });

    if (!existingOutlet || existingOutlet.isDeleted) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: 'No such outlet',
        },
        404,
      );
    }

    existingOutlet.isDeleted = true;
    await this.outletRepository.save(existingOutlet);
    this.activityService.deleteOutlet();

    return existingOutlet;
  }
}
