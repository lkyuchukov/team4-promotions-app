import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../common/decorators/roles.decorator';
import { UserRole } from '../common/enums/roles.enum';
import { RolesGuard } from '../common/guards/roles.guard';
import { OutletService } from './outlet.service';
import { UpdateOutletDTO } from '../models/outlet/update-outlet.dto';
import { CreateOutletDTO } from '../models/outlet/create-outlet.dto';

@Controller('outlets')
export class OutletController {
  constructor(private outletService: OutletService) {}

  @Get()
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard())
  async getAllOutlets() {
    return this.outletService.getAllOutlets();
  }

  @Get(':id')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard())
  async getOutlet(@Param('id') id: string) {
    return this.outletService.getOutlet(id);
  }

  @Post()
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async createOutlet(@Body() outletData: CreateOutletDTO) {
    return this.outletService.createOutlet(outletData);
  }

  @Put(':id')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async updateUser(@Param('id') id: string, @Body() outletData: UpdateOutletDTO) {
    return this.outletService.updateOutlet(id, outletData);
  }

  @Delete(':id')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async deleteOutlet(@Param('id') id: string) {
    return this.outletService.deleteOutlet(id);
  }
}
