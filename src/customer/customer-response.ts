import { CustomerEntity } from '../database/entities/customer.entity';

export async function customerToResponseObject(customer: CustomerEntity) {
  return {
    id: customer.id,
    name: customer.name,
    isDeleted: customer.isDeleted,
    createdOn: customer.createdOn,
    updatedOn: customer.updatedOn,
    outlets: await customer.outlets,
  };
}
