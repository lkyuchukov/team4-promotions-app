import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { ActivityEntity } from '../database/entities/activity.entity';
import { CustomerEntity } from '../database/entities/customer.entity';
import { UserEntity } from '../database/entities/user.entity';
import { ActivityService } from '../shared/activity.service';
import { CustomerController } from './customer.controller';
import { CustomerService } from './customer.service';

@Module({
  imports: [
    AuthModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    TypeOrmModule.forFeature([CustomerEntity, UserEntity, ActivityEntity]),
  ],
  providers: [CustomerService, ActivityService],
  controllers: [CustomerController],
})
export class CustomerModule {}
