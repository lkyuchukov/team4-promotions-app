import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../common/decorators/roles.decorator';
import { UserRole } from '../common/enums/roles.enum';
import { RolesGuard } from '../common/guards/roles.guard';
import { CreateCustomerDTO } from '../models/customer/create-customer.dto';
import { UpdateCustomerDTO } from '../models/customer/update-customer.dto';
import { CustomerService } from './customer.service';

@Controller('customers')
export class CustomerController {
  constructor(private customerService: CustomerService) {}

  @Get()
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async getAllCustomers() {
    return this.customerService.getAllCustomers();
  }

  @Get(':id')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async getCustomer(@Param('id') id: string) {
    return this.customerService.getCustomer(id);
  }

  @Post()
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async createCustomer(@Body() customerData: CreateCustomerDTO) {
    return this.customerService.createCustomer(customerData);
  }

  @Put(':name')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async updateCustomer(
    @Param('name') name: string,
    @Body() customerData: Partial<UpdateCustomerDTO>,
  ) {
    return this.customerService.updateCustomer(name, customerData);
  }

  @Delete(':id')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async deleteCustomer(@Param('id') id: string) {
    return this.customerService.deleteCustomer(id);
  }
}
