import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ActivityEntity } from '../database/entities/activity.entity';
import { CustomerEntity } from '../database/entities/customer.entity';
import { CreateCustomerDTO } from '../models/customer/create-customer.dto';
import { UpdateCustomerDTO } from '../models/customer/update-customer.dto';
import { customerToResponseObject } from './customer-response';
import { ActivityService } from '../shared/activity.service';

@Injectable()
export class CustomerService {
  constructor(
    @InjectRepository(CustomerEntity)
    private customerRepository: Repository<CustomerEntity>,

    private activityService: ActivityService,
  ) {}

  async getAllCustomers() {
    const customers = await this.customerRepository.find({
      where: { isDeleted: false },
      relations: ['outlets'],
    });

    const response = customers.map(
      async customer => await customerToResponseObject(customer),
    );
    return Promise.all(response);
  }

  async getCustomer(id: string) {
    const customer = await this.customerRepository.findOne({
      where: { id },
      relations: ['outlets'],
    });

    if (!customer || customer.isDeleted === true) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: 'No such customer',
        },
        404,
      );
    }

    return customerToResponseObject(customer);
  }

  async createCustomer(customerData: CreateCustomerDTO) {
    const { name } = customerData;

    const existingCustomer = await this.customerRepository.findOne({
      where: { name },
    });

    if (existingCustomer) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          message: 'Customer already exists.',
        },
        409,
      );
    }

    const newCustomer = new CustomerEntity();
    newCustomer.name = name;

    await this.customerRepository.save(newCustomer);
    this.activityService.createCustomer();

    return customerToResponseObject(newCustomer);
  }

  async updateCustomer(id: string, customerData: Partial<UpdateCustomerDTO>) {
    const existingCustomer = await this.customerRepository.findOne({
      where: { id },
    });

    if (!existingCustomer || existingCustomer.isDeleted) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: 'No such customer',
        },
        404,
      );
    }

    existingCustomer.name = customerData.name;
    await this.customerRepository.save(existingCustomer);
    this.activityService.editCustomer();

    return customerToResponseObject(existingCustomer);
  }

  async deleteCustomer(id: string) {
    const existingCustomer = await this.customerRepository.findOne({
      where: { id },
    });

    if (!existingCustomer || existingCustomer.isDeleted) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: 'No such customer',
        },
        404,
      );
    }
    existingCustomer.isDeleted = true;

    await this.customerRepository.save(existingCustomer);
    this.activityService.deleteCustomer();

    return customerToResponseObject(existingCustomer);
  }
}
