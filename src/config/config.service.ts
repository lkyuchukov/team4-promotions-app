import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as Joi from 'joi';
import { DatabaseType } from 'typeorm';

export interface EnvConfig {
  [key: string]: string;
}

@Injectable()
export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor(filePath: string = null) {
    let config;
    if (filePath) {
      config = dotenv.parse(fs.readFileSync(filePath));
    } else {
      config = dotenv.config().parsed;
    }
    this.envConfig = this.validateInput(config);
  }

  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['development', 'production', 'test', 'provision'])
        .default('development'),
      SOURCE_PATH: Joi.string().default('src'),
      PORT: Joi.number().default(3000),
      JWT_SECRET: Joi.string().required(),
      JWT_EXPIRE: Joi.number().default(3600 * 24 * 7),
      TYPEORM_CONNECTION: Joi.string().default('mysql'),
      TYPEORM_HOST: Joi.string().default('localhost'),
      TYPEORM_PORT: Joi.number().default(3306),
      TYPEORM_USERNAME: Joi.string().default('root'),
      TYPEORM_PASSWORD: Joi.string().default('root'),
      TYPEORM_DATABASE: Joi.string().required(),
      TYPEORM_ENTITIES: Joi.string().required(),
      TYPEORM_MIGRATIONS: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }

  public get environment(): string {
    return this.envConfig.NODE_ENV;
  }

  public get sourcePath(): string {
    return this.envConfig.SOURCE_PATH;
  }

  public get port(): number {
    return +this.envConfig.PORT;
  }

  public get jwtSecret(): string {
    return this.envConfig.JWT_SECRET;
  }

  public get jwtExpireTime(): number {
    return +this.envConfig.JWT_EXPIRE;
  }

  public get dbHost(): string {
    return this.envConfig.TYPEORM_HOST;
  }

  public get dbPort(): number {
    return +this.envConfig.TYPEORM_PORT;
  }

  public get dbUsername(): string {
    return this.envConfig.TYPEORM_USERNAME;
  }

  public get dbPassword(): string {
    return this.envConfig.TYPEORM_PASSWORD;
  }

  public get dbName(): string {
    return this.envConfig.TYPEORM_DATABASE;
  }

  public get dbType(): DatabaseType {
    return this.envConfig.TYPEORM_CONNECTION as DatabaseType;
  }
}
