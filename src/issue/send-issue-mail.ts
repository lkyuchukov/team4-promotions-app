import { Logger } from '@nestjs/common';
import * as nodemailer from 'nodemailer';
import { SubmitIssueDTO } from '../models/issue/submit-issue';
import { UserDTO } from '../models/user/user';

export async function sendIssueMail(
  userData: UserDTO,
  emailData: SubmitIssueDTO,
) {
  const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    auth: {
      user: 'judge.becker@ethereal.email',
      pass: 'VVDK8sW3upgMJ5hyam',
    },
  });

  const info = await transporter.sendMail({
    from: `${userData.username}@promotions.com`,
    to: 'support@promotions.com',
    subject: emailData.title,
    text: emailData.description,
  });

  Logger.log('Message sent: %s', info.messageId);
}
