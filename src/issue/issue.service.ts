import { Injectable, Logger } from '@nestjs/common';
import { SubmitIssueDTO } from '../models/issue/submit-issue';
import { UserDTO } from '../models/user/user';
import { sendIssueMail } from './send-issue-mail';

@Injectable()
export class IssueService {
  async submitIssue(userData: UserDTO, issueData: SubmitIssueDTO) {
    await sendIssueMail(userData, issueData).catch(Logger.error);
  }
}
