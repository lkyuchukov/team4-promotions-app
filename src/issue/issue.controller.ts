import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserDecorator } from '../common/decorators/user.decorator';
import { SubmitIssueDTO } from '../models/issue/submit-issue';
import { UserDTO } from '../models/user/user';
import { IssueService } from './issue.service';

@Controller('issues')
export class IssueController {
  constructor(private issueService: IssueService) {}

  @Post()
  @UseGuards(AuthGuard())
  async submitIssue(
    @UserDecorator() userData: UserDTO,
    @Body() issueData: SubmitIssueDTO,
  ) {
    return this.issueService.submitIssue(userData, issueData);
  }
}
