import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { ConfigModule } from '../config/config.module';
import { ActivityEntity } from '../database/entities/activity.entity';
import { UserEntity } from '../database/entities/user.entity';
import { ActivityService } from '../shared/activity.service';
import { IssueController } from './issue.controller';
import { IssueService } from './issue.service';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    AuthModule,
    TypeOrmModule.forFeature([UserEntity, ActivityEntity]),
    ConfigModule,
  ],
  providers: [IssueService, ActivityService],
  controllers: [IssueController],
})
export class IssueModule {}
