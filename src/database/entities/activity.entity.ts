import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ActivityEnum } from '../../common/enums/activity.enum';
import { EntityEnum } from '../../common/enums/entity.enum';
import { UserEntity } from './user.entity';

@Entity('activity')
export class ActivityEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn()
  createdOn: Date;

  @Column({ type: 'enum', enum: EntityEnum })
  entityType: EntityEnum;

  @Column({ type: 'enum', enum: ActivityEnum })
  action: ActivityEnum;

  @ManyToOne(type => UserEntity)
  user: Promise<UserEntity>;
}
