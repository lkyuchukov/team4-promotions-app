import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PrizeEntity } from './prize.entity';

@Entity('code')
export class CodeEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  value: string;

  @Column({ default: false })
  isRedeemed: boolean;

  @Column({ default: false })
  redemptionCancelled: boolean;

  @Column({ default: false })
  isDeleted: boolean;

  @OneToOne(type => PrizeEntity, { eager: true })
  @JoinColumn()
  prize: PrizeEntity;
}
