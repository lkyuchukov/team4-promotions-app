import { Column, Entity, OneToMany, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { OutletEntity } from './outlet.entity';

@Entity('customer')
export class CustomerEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  name: string;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @Column({ default: false })
  isDeleted: boolean;

  @OneToMany(type => OutletEntity, outlet => outlet.customer)
  outlets: Promise<OutletEntity[]>;
}
