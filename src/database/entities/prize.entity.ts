import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { PrizeEnum } from '../../common/enums/prize.enum';

@Entity('prize')
export class PrizeEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'enum', enum: PrizeEnum, default: PrizeEnum.Small })
  prize: string;
}
