import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CustomerEntity } from './customer.entity';
import { RedemptionRecordEntity } from './redemption-record.entity';
import { UserEntity } from './user.entity';

@Entity('outlet')
export class OutletEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  address: string;

  @Column('nvarchar')
  city: string;

  @OneToMany(type => UserEntity, user => user.outlet)
  users: Promise<UserEntity[]>;

  @Column({ default: false })
  isDeleted: boolean;

  @OneToMany(
    type => RedemptionRecordEntity,
    redemptionRecord => redemptionRecord.outlet,
  )
  redemptionRecords: Promise<RedemptionRecordEntity[]>;

  @ManyToOne(type => CustomerEntity, customer => customer.outlets)
  customer: Promise<CustomerEntity>;
}
