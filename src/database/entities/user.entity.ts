import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ActivityEntity } from './activity.entity';
import { OutletEntity } from './outlet.entity';
import { RedemptionRecordEntity } from './redemption-record.entity';
import { RoleEntity } from './role.entity';

@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @Column('nvarchar', { unique: true })
  username: string;

  @Column('nvarchar')
  password: string;

  @Column({ default: false })
  isLoggedIn: boolean;

  @Column({ default: false })
  isDeleted: boolean;

  @ManyToMany(type => RoleEntity, { eager: true })
  @JoinTable()
  roles: RoleEntity[];

  @OneToMany(
    type => RedemptionRecordEntity,
    redemptionRecord => redemptionRecord.user,
  )
  redemptionRecords: Promise<RedemptionRecordEntity[]>;

  @ManyToOne(type => OutletEntity, outlet => outlet.users)
  outlet: Promise<OutletEntity>;

  @OneToMany(type => ActivityEntity, activity => activity.user)
  activity: Promise<ActivityEntity[]>;
}
