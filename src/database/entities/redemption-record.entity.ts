import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CodeEntity } from './code.entity';
import { OutletEntity } from './outlet.entity';
import { UserEntity } from './user.entity';

@Entity('redemptionRecord')
export class RedemptionRecordEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(type => CodeEntity)
  @JoinColumn()
  code: Promise<CodeEntity>;

  @CreateDateColumn()
  createdOn: Date;

  @Column({ default: false })
  isDeleted: boolean;

  @Column('datetime', { default: null })
  redeemedOn: Date;

  @ManyToOne(type => UserEntity, user => user.redemptionRecords)
  user: Promise<UserEntity>;

  @ManyToOne(type => OutletEntity, outlet => outlet.redemptionRecords)
  outlet: Promise<OutletEntity>;
}
