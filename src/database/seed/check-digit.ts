export function checksum(code: any) {
  const sum = code
    .split('')
    .reverse()
    .reduce((sum: any, char: any, idx: any) => {
      const digit = Number.parseInt(char);
      const weight = (idx + 1) % 2 === 0 ? 1 : 3;
      const partial = digit * weight;
      return sum + partial;
    }, 0);

  const remainder = sum % 10;
  const checksum = remainder ? 10 - remainder : 0;

  return checksum;
}
