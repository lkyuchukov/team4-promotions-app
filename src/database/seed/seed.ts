import * as bcrypt from 'bcrypt';
import * as faker from 'faker';
import { createConnection, Repository } from 'typeorm';
import { PrizeEnum } from '../../common/enums/prize.enum';
import { UserRole } from '../../common/enums/roles.enum';
import { CodeEntity } from '../entities/code.entity';
import { CustomerEntity } from '../entities/customer.entity';
import { OutletEntity } from '../entities/outlet.entity';
import { PrizeEntity } from '../entities/prize.entity';
import { RoleEntity } from '../entities/role.entity';
import { UserEntity } from '../entities/user.entity';
import { checksum } from './check-digit';

const seedRoles = async connection => {
  const rolesRepository: Repository<
    RoleEntity
  > = connection.manager.getRepository(RoleEntity);

  const roles: RoleEntity[] = await rolesRepository.find();
  if (roles.length) {
    console.log('The DB already has roles');
    return;
  }

  const rolesSeeding: Array<Promise<RoleEntity>> = Object.keys(UserRole).map(
    async (roleName: string) => {
      const role: RoleEntity = rolesRepository.create({ name: roleName });
      return await rolesRepository.save(role);
    },
  );
  await Promise.all(rolesSeeding);

  console.log('Seeded roles successfully');
};

const seedAdmin = async connection => {
  const userRepository: Repository<
    UserEntity
  > = connection.manager.getRepository(UserEntity);
  const rolesRepository: Repository<
    RoleEntity
  > = connection.manager.getRepository(RoleEntity);

  const outletRepository: Repository<
    OutletEntity
  > = connection.manager.getRepository(OutletEntity);

  const allOutlets = await outletRepository.find({});

  const admin = await userRepository.findOne({
    where: {
      name: 'admin',
    },
  });

  if (admin) {
    console.log('The DB already has an admin');
    return;
  }

  const adminRole: RoleEntity = await rolesRepository.findOne({
    name: UserRole.Admin,
  });
  if (!adminRole) {
    console.log('The DB does not have an admin role');
    return;
  }

  const newAdmin = new UserEntity();
  newAdmin.username = 'admin';
  newAdmin.password = 'password';
  newAdmin.roles = [adminRole];
  newAdmin.outlet = Promise.resolve(
    allOutlets[Math.floor(Math.random() * allOutlets.length)],
  );
  const passwordHash = await bcrypt.hash(newAdmin.password, 10);
  newAdmin.password = passwordHash;

  await userRepository.save(newAdmin);
  console.log('Seeded admin successfully');
};

const seedPrizes = async connection => {
  const prizeRepository: Repository<
    PrizeEntity
  > = connection.manager.getRepository(PrizeEntity);

  const codeRepository: Repository<
    CodeEntity
  > = connection.manager.getRepository(CodeEntity);

  for (let i = 0; i < 25; i++) {
    const fakePrize = new PrizeEntity();
    fakePrize.prize = PrizeEnum.Small;

    await prizeRepository.save(fakePrize);

    const fakeCode = new CodeEntity();
    const code = faker.random
      .number({
        min: 100000000000,
        max: 999999999999,
      })
      .toString();

    fakeCode.value = code + checksum(code);
    fakeCode.prize = fakePrize;

    await codeRepository.save(fakeCode);
  }

  for (let i = 0; i < 25; i++) {
    const fakePrize = new PrizeEntity();
    fakePrize.prize = PrizeEnum.Medium;

    await prizeRepository.save(fakePrize);

    const fakeCode = new CodeEntity();
    const code = faker.random
      .number({
        min: 100000000000,
        max: 999999999999,
      })
      .toString();

    fakeCode.value = code + checksum(code);
    fakeCode.prize = fakePrize;

    await codeRepository.save(fakeCode);
  }

  for (let i = 0; i < 25; i++) {
    const fakePrize = new PrizeEntity();
    fakePrize.prize = PrizeEnum.Big;

    await prizeRepository.save(fakePrize);

    const fakeCode = new CodeEntity();
    const code = faker.random
      .number({
        min: 100000000000,
        max: 999999999999,
      })
      .toString();

    fakeCode.value = code + checksum(code);
    fakeCode.prize = fakePrize;

    await codeRepository.save(fakeCode);
  }

  console.log('Seeded prizes successfully.');
};

const seedUsers = async connection => {
  const userRepository: Repository<
    UserEntity
  > = connection.manager.getRepository(UserEntity);
  const rolesRepository: Repository<
    RoleEntity
  > = connection.manager.getRepository(RoleEntity);
  const basicRole: RoleEntity = await rolesRepository.findOne({
    name: UserRole.Basic,
  });
  const outletRepository: Repository<
    OutletEntity
  > = connection.manager.getRepository(OutletEntity);

  const users = [];
  const allOutlets = await outletRepository.find({});

  for (let i = 0; i < 600; i++) {
    const fakeUser = new UserEntity();
    fakeUser.username = faker.internet.userName();
    fakeUser.password = 'password';
    fakeUser.roles = [basicRole];
    fakeUser.outlet = Promise.resolve(
      allOutlets[Math.floor(Math.random() * allOutlets.length)],
    );
    users.push(fakeUser);

    fakeUser.password = await bcrypt.hash(fakeUser.password, 10);
    await userRepository.save(fakeUser);
  }

  console.log('Seeded users successfully.');
};

const seedCustomers = async connection => {
  const customerRepository: Repository<
    CustomerEntity
  > = connection.manager.getRepository(CustomerEntity);

  for (let i = 0; i < 100; i++) {
    const fakeCustomer = new CustomerEntity();
    fakeCustomer.name = faker.company.companyName();

    await customerRepository.save(fakeCustomer);
  }
  console.log('Seeded customers successfully');
};

const seedOutlets = async connection => {
  const outletRepository: Repository<
    OutletEntity
  > = connection.manager.getRepository(OutletEntity);

  const customerRepository: Repository<
    CustomerEntity
  > = connection.manager.getRepository(CustomerEntity);

  const allCustomers = await customerRepository.find({});

  for (let i = 0; i < 300; i++) {
    const fakeOutlet = new OutletEntity();
    fakeOutlet.address = faker.address.streetAddress();
    fakeOutlet.customer = Promise.resolve(
      allCustomers[Math.floor(Math.random() * allCustomers.length)],
    );
    fakeOutlet.city = faker.address.city();
    await outletRepository.save(fakeOutlet);
  }
  console.log('Seeded outlets successfully');
};

// const seedRedemptionRecords = async connection => {
//   const redemptionRecordRepository: Repository<
//     RedemptionRecordEntity
//   > = connection.manager.getRepository(RedemptionRecordEntity);

//   const codeRepository: Repository<
//     CodeEntity
//   > = connection.manager.getRepository(CodeEntity);

//   const userRepository: Repository<
//     UserEntity
//   > = connection.manager.getRepository(UserEntity);

//   const allUsers = await userRepository.find({});
//   const allCodes = await codeRepository.find({});

//   for (let i = 50; i < 90; i++) {
//     const fakeRedemption = new RedemptionRecordEntity();
//     fakeRedemption.code = Promise.resolve(allCodes[i]);
//     allCodes[i].isRedeemed = true;
//     fakeRedemption.user = Promise.resolve(allUsers[i]);
//     fakeRedemption.redeemedOn = faker.date.recent();
//     fakeRedemption.outlet = Promise.resolve(await allUsers[i].outlet);

//     await codeRepository.save(allCodes[i]);
//     await redemptionRecordRepository.save(fakeRedemption);
//   }
//   console.log('Seeded redemptions successfully');
// };

const seed = async () => {
  console.log('Seed started');
  const connection = await createConnection();

  await seedRoles(connection);
  await seedPrizes(connection);
  await seedCustomers(connection);
  await seedOutlets(connection);
  await seedAdmin(connection);
  await seedUsers(connection);
  // this is exprimental
  // await seedRedemptionRecords(connection);

  await connection.close();
  console.log('Seed completed');
};

seed().catch(console.error);
