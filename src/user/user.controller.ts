import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../common/decorators/roles.decorator';
import { UserRole } from '../common/enums/roles.enum';
import { RolesGuard } from '../common/guards/roles.guard';
import { CreateUserDTO } from '../models/user/create-user';
import { UpdateUserDTO } from '../models/user/update-user';
import { UserService } from './user.service';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async getAllUsers() {
    return this.userService.getAllUsers();
  }

  @Post()
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async createUser(@Body() userData: CreateUserDTO) {
    return this.userService.createUser(userData);
  }

  @Get(':id')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async getUser(@Param('id') id: string) {
    return this.userService.getUser(id);
  }

  @Put(':id')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async updateUser(@Param('id') id: string, @Body() userData: UpdateUserDTO) {
    return this.userService.updateUser(id, userData);
  }

  @Delete(':id')
  @Roles(UserRole.Admin)
  @UseGuards(AuthGuard(), RolesGuard)
  async deleteUser(@Param('id') id: string) {
    return this.userService.deleteUser(id);
  }
}
