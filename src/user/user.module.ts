import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { ActivityEntity } from '../database/entities/activity.entity';
import { OutletEntity } from '../database/entities/outlet.entity';
import { RoleEntity } from '../database/entities/role.entity';
import { UserEntity } from '../database/entities/user.entity';
import { ActivityService } from '../shared/activity.service';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    AuthModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    TypeOrmModule.forFeature([
      UserEntity,
      OutletEntity,
      RoleEntity,
      ActivityEntity,
    ]),
  ],
  providers: [UserService, ActivityService],
  controllers: [UserController],
})
export class UserModule {}
