import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { UserRole } from '../common/enums/roles.enum';
import { OutletEntity } from '../database/entities/outlet.entity';
import { RoleEntity } from '../database/entities/role.entity';
import { UserEntity } from '../database/entities/user.entity';
import { CreateUserDTO } from '../models/user/create-user';
import { UpdateUserDTO } from '../models/user/update-user';
import { ActivityService } from '../shared/activity.service';
import { userToResponseObject } from './user-response';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(OutletEntity)
    private outletRepository: Repository<OutletEntity>,

    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,

    @InjectRepository(RoleEntity)
    private rolesRepository: Repository<RoleEntity>,

    private activityService: ActivityService,
  ) {}

  async getAllUsers() {
    const users = await this.userRepository.find({
      where: { isDeleted: false },
      relations: ['outlet'],
    });

    const response = users.map(async user => await userToResponseObject(user));

    return Promise.all(response);
  }

  async getUser(id: string) {
    const user = await this.userRepository.findOne({
      where: { id },
      relations: ['redemptionRecords'],
    });

    if (!user || user.isDeleted === true) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: 'No such user',
        },
        404,
      );
    }

    return userToResponseObject(user);
  }

  async createUser(userData: CreateUserDTO) {
    const { username, password, outletId } = userData;

    const existingUser = await this.userRepository.findOne({
      where: { username },
    });

    if (existingUser) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          message: 'User exists, but account is deactivated',
        },
        409,
      );
    }

    const basicRole: RoleEntity = await this.rolesRepository.findOne({
      name: UserRole.Basic,
    });

    const newUser = new UserEntity();
    newUser.username = username;
    newUser.password = await bcrypt.hash(password, 10);
    newUser.roles = [basicRole];

    const userOutlet = await this.outletRepository.findOne({
      where: { id: outletId },
    });

    if (!userOutlet) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: 'Outlet does not exist',
        },
        404,
      );
    }

    newUser.outlet = Promise.resolve(userOutlet);

    await this.userRepository.save(newUser);
    this.activityService.createUser();

    return userToResponseObject(newUser);
  }

  async updateUser(id: string, userData: Partial<UpdateUserDTO>) {
    const { username, password, outletName } = userData;

    const existingUser = await this.userRepository.findOne({
      where: { id },
    });

    if (!existingUser) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: 'No such user',
        },
        404,
      );
    }

    if (username) {
      existingUser.username = username;
    }

    if (password) {
      existingUser.password = await bcrypt.hash(password, 10);
    }

    if (outletName) {
      const newOutlet = await this.outletRepository.findOne({
        where: { address: outletName },
      });
      existingUser.outlet = Promise.resolve(newOutlet);
    }

    await this.userRepository.save(existingUser);
    this.activityService.editUser();

    return userToResponseObject(existingUser);
  }

  async deleteUser(id: string) {
    const existingUser = await this.userRepository.findOne({
      where: { id },
    });

    if (!existingUser) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          message: 'No such user',
        },
        404,
      );
    }

    existingUser.isDeleted = true;

    await this.userRepository.save(existingUser);
    this.activityService.deleteUser();

    return userToResponseObject(existingUser);
  }
}
