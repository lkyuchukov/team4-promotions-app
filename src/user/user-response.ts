import { UserEntity } from '../database/entities/user.entity';

export async function userToResponseObject(user: UserEntity) {
  return {
    id: user.id,
    username: user.username,
    createdOn: user.createdOn,
    updatedOn: user.updatedOn,
    isDeleted: user.isDeleted,
    roles: user.roles,
    redemptionRecords: await user.redemptionRecords,
    outlet: await user.outlet,
  };
}
