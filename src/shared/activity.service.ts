import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ActivityEnum } from '../common/enums/activity.enum';
import { EntityEnum } from '../common/enums/entity.enum';
import { ActivityEntity } from '../database/entities/activity.entity';
import { UserEntity } from '../database/entities/user.entity';

@Injectable()
export class ActivityService {
  constructor(
    @InjectRepository(ActivityEntity)
    private activityRepository: Repository<ActivityEntity>,

    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async getAdmin() {
    const admin = await this.userRepository.findOne({
      where: { username: 'admin' },
    });

    return await admin;
  }

  async redeemCode(user: UserEntity) {
    const activity = new ActivityEntity();
    activity.entityType = EntityEnum.Code;
    activity.user = Promise.resolve(user);
    activity.action = ActivityEnum.Redeem;

    await this.activityRepository.save(activity);
  }

  async reportMultipleRedemptionAttempt(user: UserEntity) {
    const activity = new ActivityEntity();
    activity.entityType = EntityEnum.MultipleRedemptionAttempt;
    activity.user = Promise.resolve(user);
    activity.action = ActivityEnum.Report;

    await this.activityRepository.save(activity);
  }

  async createOutlet() {
    const activity = new ActivityEntity();
    activity.entityType = EntityEnum.Customer;
    activity.user = Promise.resolve(await this.getAdmin());
    activity.action = ActivityEnum.Create;

    await this.activityRepository.save(activity);
  }

  async editOutlet() {
    const activity = new ActivityEntity();
    activity.entityType = EntityEnum.Outlet;
    activity.user = Promise.resolve(await this.getAdmin());
    activity.action = ActivityEnum.Update;

    await this.activityRepository.save(activity);
  }

  async deleteOutlet() {
    const activity = new ActivityEntity();
    activity.entityType = EntityEnum.Outlet;
    activity.user = Promise.resolve(await this.getAdmin());
    activity.action = ActivityEnum.Delete;

    await this.activityRepository.save(activity);
  }

  async createCustomer() {
    const activity = new ActivityEntity();
    activity.entityType = EntityEnum.Customer;
    activity.user = Promise.resolve(await this.getAdmin());
    activity.action = ActivityEnum.Create;

    await this.activityRepository.save(activity);
  }

  async editCustomer() {
    const activity = new ActivityEntity();
    activity.entityType = EntityEnum.Customer;
    activity.user = Promise.resolve(await this.getAdmin());
    activity.action = ActivityEnum.Update;

    await this.activityRepository.save(activity);
  }

  async deleteCustomer() {
    const activity = new ActivityEntity();
    activity.entityType = EntityEnum.Customer;
    activity.user = Promise.resolve(await this.getAdmin());
    activity.action = ActivityEnum.Delete;

    await this.activityRepository.save(activity);
  }

  async createUser() {
    const activity = new ActivityEntity();
    activity.entityType = EntityEnum.User;
    activity.user = Promise.resolve(await this.getAdmin());
    activity.action = ActivityEnum.Create;

    await this.activityRepository.save(activity);
  }

  async editUser() {
    const activity = new ActivityEntity();
    activity.entityType = EntityEnum.User;
    activity.user = Promise.resolve(await this.getAdmin());
    activity.action = ActivityEnum.Update;

    await this.activityRepository.save(activity);
  }

  async deleteUser() {
    const activity = new ActivityEntity();
    activity.entityType = EntityEnum.User;
    activity.user = Promise.resolve(await this.getAdmin());
    activity.action = ActivityEnum.Delete;

    await this.activityRepository.save(activity);
  }
}
