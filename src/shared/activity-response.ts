import { ActivityEntity } from '../database/entities/activity.entity';

export async function activityResponse(activity: ActivityEntity) {
  return {
    id: activity.id,
    action: activity.action,
    createdOn: activity.createdOn,
    user: await activity.user,
    entityType: activity.entityType,
  };
}
